<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function create(){
			$rules = array(array(
					'field' => 'name',
					'label' => 'Nome',
					'rules' => 'required'
			),array(
				'field' => 'mail',
				'label' => 'E-mail',
				'rules' => 'trim|required|valid_email'
			));
			$this->form_validation->set_rules($rules);
			if($this->form_validation->run() === FALSE){
				return $this->output
						->set_content_type('application/json')
						->set_status_header(400)
						->set_output(json_encode(array(
						'status' => 400,
						'message' => 'Não foi possível processar seu pedido'
				)));
			}

			return $this->output
					->set_content_type('application/json')
					->set_status_header(200)
					->set_output(json_encode(array(
					'status' => 200,
					'message' => $this->input->post()
			)));
	}
}
