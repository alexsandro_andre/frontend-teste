<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="<?php echo site_url(); ?>dist/app.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet">
    <title>teste</title>
  </head>
    <body>
      <header class="container-fluid">

            <div class="row">
                <div id="logo-holder" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    <img alt="Eleve CRM" title="Eleve CRM" src="img/bitmap.png">
                </div>
            </div>

            <div class="row">
                <div id="main-info-holder" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <h1 class="text-center">Ótima escolha!</h1>

                    <div class="subtitle">
                        <h2 class="text-center">Crie sua conta <strong>gratuita</strong> e amplie seus resultados.</h2>
                        <h2 class="text-center">EleveCRM é a forma mais eficaz de elevar os resultados de seu time comercial!</h2>
                    </div>
                </div>
            </div>

            <div id="sub-info-holder" class="row">
                <div id="mkt-holder" class="col-md-4 col-lg-6 col-sm-6 d-sm-none d-none d-sm-block">
                    <img class="img-responsive col-md-12" alt="Plataforma Eleve" title="Plataforma Eleve" src="img/image.png">
                </div>
                <div class="col-md-7 col-lg-4 col-sm-5 d-sm-none d-none d-sm-block">
                    <div id="form-holder">
                        <form method="POST" class="form-contato form-required" action="#">
                            <fieldset>
                                <div class="form-group">
                                    <label>Dados pessoais</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control required" placeholder="Nome">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="mail" id="mail" class="form-control required" placeholder="E-mail">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="pass" id="pass" class="form-control required" placeholder="Senha">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="confirm_pass" id="confirm_pass" class="form-control required" placeholder="Confirmação de senha">
                                </div>
                                <small id="password-reminder" class="text-muted hide">A senha e a confirmação devem ser iguais</small>
                            </fieldset>

                            <fieldset>
                                <div class="form-group">
                                    <label>Dados da empresa</label>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="cnpj" id="id_cnpj" class="form-control required" placeholder="CNPJ">
                                </div>
                                <small id="cnpj-reminder" class="text-muted hide">A senha e a confirmação devem ser iguais</small>
                                <div class="form-group">
                                    <input type="text" name="razao_social" id="id_razao_social" class="form-control required" placeholder="Razão social">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="nome_fantasia" id="id_nome_fantasia" class="form-control required" placeholder="Nome fantasia">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="site" id="id_site" class="form-control required" placeholder="Site">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="telefone" id="id_telefone" class="form-control required" placeholder="Telefone">
                                </div>
                            </fieldset>

                            <div class="row" id="submit-opt">
                                <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="termos" id="id_termos"> Concordo com os <a href="#">termos de serviço</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                                    <input type="submit" id="submit_1" class="btn btn-primary disabled" value="Criar conta">
                                    <small id="reminder" class="text-muted hide">Atenção aos campos obrigatórios</small>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </header>

        <section id="form-container" class="container-fluid d-sm-none d-sm-block d-xl-none d-md-none">
            <div id="form-mobile-holder" class="row">
                <form method="POST" class="form-contato form-required" action="#">
                    <fieldset>
                        <div class="form-group">
                            <label>Dados pessoais</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="nome" id="nome" class="form-control required" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control required" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <input type="password" name="senha" id="senha" class="form-control required" placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <input type="password" name="senha_confirme" id="senha_confirme" class="form-control required" placeholder="Confirmação de senha">
                        </div>
                        <small id="password-reminder2" class="text-muted hide password-reminder">A senha e a confirmação devem ser iguais</small>
                    </fieldset>

                    <fieldset>
                        <div class="form-group">
                            <label>Dados da empresa</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="cnpj" id="cnpj" class="form-control required" placeholder="CNPJ">
                        </div>
                        <div class="form-group">
                            <input type="text" name="razao_social" id="razao_social" class="form-control required" placeholder="Razão social">
                        </div>
                        <div class="form-group">
                            <input type="text" name="nome_fantasia" id="nome_fantasia" class="form-control required" placeholder="Nome fantasia">
                        </div>
                        <div class="form-group">
                            <input type="text" name="site" id="site" class="form-control required" placeholder="Site">
                        </div>
                        <div class="form-group">
                            <input type="text" name="telefone" id="telefone" class="form-control required" placeholder="Telefone">
                        </div>
                    </fieldset>

                    <div class="row" id="submit-opt">
                        <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="termos" id="termos"> Concordo com os <a href="#">termos de serviço</a>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                            <input type="submit" id="submit" class="btn btn-primary" value="Criar conta">
                            <small id="reminder" class="text-muted hide">Atenção aos campos obrigatórios</small>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <section id="sub-container" class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-lg-6 col-sm-6 d-sm-none d-none d-sm-block">
                    <div id="beta-info-holder">
                        <p class="col-md-12">
                        Você está se cadastrando em nova versão beta, totalmente gratuita por tempo indeterminado.
                        Aproveite e veja como é fácil gerenciar suas vendas e seu time comercial usando o Eleve CRM.
                        </p>
                        <div class="divider"></div>
                    </div>

                </div>
            </div>

        </section>

        <footer class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-3 col-md-3">
                        <div class="box-header">
                            <h3>Mapa de Navegação</h3>
                        </div>
                        <div class="box-content">
                            <ul class="nav-list">
                                <li>
                                    <a href="#">Conheça</a>
                                </li>
                                <li>
                                    <a href="#">Planos</a>
                                </li>
                                <li>
                                    <a href="#">Blog</a>
                                </li>
                                <li>
                                    <a href="#">Sou cliente</a>
                                </li>
                                <li>
                                    <a href="#">Cadastre-se</a>
                                </li>
                                <li>
                                    <a href="#">Contato</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="box-header">
                            <h3>Sobre nós</h3>
                        </div>
                        <div class="box-content">
                            <p>
                                O ELEVEcrm foi concebido, objetivando acelerar o processo comercial,
                                integrar práticas e formalizar etapas de atendimento, permitindo formatar
                                e disponibilizar documentos, formulários e e-mails padrões, e acompanhar
                                todo o ciclo comercial, com leveza, simplicidade e muito gerenciamento
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-md-5">
                        <div class="box-header">
                            <h3>Últimas do blog</h3>
                        </div>
                        <div class="box-content">
                            <div class="blog-list-item">
                                <a href="#">O que seria Customer Success?</a>
                                <br>
                                <small class="text-muted">12/07/2017</small>
                            </div>
                            <div class="blog-list-item">
                                <a href="#">A boa entrega é a garantia da próxima venda!</a>
                                <br>
                                <small class="text-muted">12/07/2017</small>
                            </div>
                            <div class="blog-list-item last-item">
                                <a href="#">Afinal o que são vendas complexas?</a>
                                <br>
                                <small class="text-muted">12/07/2017</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-md-12 text-center">
                        <div id="logo-footer-holder">
                            <img src="img/logo-footer.png" alt="Eleve CRM" title="Eleve CRM">
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    <script src="<?php echo site_url(); ?>dist/bundle.js?<?php echo date('l jS \of F Y h:i:s A'); ?>" charset="utf-8"></script>
  </body>
</html>
