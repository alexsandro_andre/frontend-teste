import 'jquery';
import 'jquery-mask-plugin';
import 'bootstrap';


window.$ = window.jQuery = require('jquery');


$(function(){
  $('#password-reminder').hide();
  $("#password-reminder2").hide();
  $("#cnpj-reminder").hide();
  $("#name").focus();
  $('input[name="telefone"]').mask('(99) 9999-99999');
  $('input[name="cnpj"]').mask('99.999.999/9999-99');

  $("#submit_1").click(function(){
    validateForm();
        
    $.ajax({
      method: "POST",
      url: "welcome/create",
      data: deskData()
    }).done(function( success ) {
        console.log( "Cadastrado com sucesso " , success );
      });
      return false;
    });

  $("#submit").click(function(){
    validateForm();
    return false;
  });

  $("#termos").click(function(){
    $("#submit_1").removeClass('disabled');
  });

  regularizeFields();
  checkPassword();
  checkPasswordMobile();
});

const deskData = function(){
  return {
    name : $("#name").val(),
    mail : $("#mail").val(),
    pass : $("#pass").val(),
    id_cnpj : $("#id_cnpj").val(),
    razao_social : $("#id_razao_social").val(),
    nome_fantasia : $("#id_nome_fantasia").val(),
    site : $("#id_site").val(),
    id_telefone : $("#id_telefone").val()
  }
}

/**
 * [click submit form validate]
 * @return {[type]} [false]
 */
const validateForm = function(){
  $('.form-required').find(':input').each(function(k,v){
    if($(this).hasClass('required') === true && $(this).val() === ''){
      $(this).addClass('was-validate-custom');
    }
  });
}

/**
 * [check validation in form events keypress keyup blur and focus]
 * @return {[type]} [false]
 */
const regularizeFields = function(){
  $(':input').bind('keypress keyup blur focus', function(e){
    if(e.type === 'blur'){
      if($(this).hasClass('required') === true && $(this).val() === ''){
        $(this).addClass('was-validate-custom');
      }
      return false;
    }
    $(this).removeClass('was-validate-custom');
  });
}

const checkPassword = function(){
  $('#confirm_pass').blur(function(){
    if($('#pass').val() != $(this).val()){
      $(this).focus();
      $('#password-reminder').show();
      return false;
    }
    $('#password-reminder').hide();
  })
}

const checkPasswordMobile = function(){
  $('#senha_confirme').blur(function(){
    if($('#senha').val() != $(this).val()){
      $(this).focus();
      $('#password-reminder2').show();
      return false;
    }
    $('#password-reminder2').hide();
  })
}
