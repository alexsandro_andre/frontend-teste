var path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const ExtractCss = new ExtractTextPlugin({
  filename: 'app.css',
  disable: false,
  allChunks: true
});

const uglyFyJs = new UglifyJSPlugin({
  test: /\.js($|\?)/i
});

module.exports ={
  entry:{
     app: ['./src/js/app.js', './src/css/app.scss']
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractCss.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader'],
          publicPath: path.resolve(__dirname, 'dist')
        })
      },
      {
         test: /\.(png|svg|jpg|gif)$/,
         use: [
           'file-loader'
         ]
       }
    ]
  },
  plugins: [ uglyFyJs, ExtractCss ]
}
